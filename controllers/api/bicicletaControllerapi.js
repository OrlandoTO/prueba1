let Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = (req,res)=>{
    res.status(200).json({
        bicicletas : Bicicleta.allBicicletas
    })
}


exports.bicicleta_create = (req,res)=>{
    let {id,color,modelo,coordenadaX,coordenadaY} = req.body
    var Bici = new Bicicleta(id,color,modelo,[coordenadaX,coordenadaY])
    Bicicleta.add(Bici);
    res.status(200).json({
        bicicleta:Bici
    })
}


exports.bicicleta_delete =(req,res)=>{
    Bicicleta.removeBici(req.body.id)
    res.status(204).send()
}



exports.bicicleta_update = (req,res)=>{
    let {id,color,modelo,coordenadaX,coordenadaY} = req.body
    var Bici = new Bicicleta(id,color,modelo,[coordenadaX,coordenadaY])
    
    Bicicleta.updateBici(Bici)
    res.status(200).json({
        bicicleta:Bici
    })
}