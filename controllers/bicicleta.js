let bicicleta = require('../models/bicicleta')
const Bicicleta = require('../models/bicicleta')


exports.bicicleta_list = (req,res)=>{
    res.render ('bicicletas/index',{bicicletas:bicicleta.allBicicletas})
}


exports.bicicleta_create = (req,res)=>{
    res.render ('bicicletas/create');
}


exports.bicicleta_store = (req,res)=>{
    let {id,color,modelo,coordenadaX,coordenadaY} = req.body;
    let bici = new Bicicleta(id,color,modelo,[coordenadaX,coordenadaY])
    Bicicleta.add(bici)
    res.redirect('/bicicleta')

}

exports.bicicleta_show =(req,res)=>{
    let id = req.params.id;
    res.render('bicicletas/update',{bicicleta:Bicicleta.findById(id)})
    
}


exports.bicicleta_update =(req,res)=>{
  
    let {id,color,modelo,coordenadaX,coordenadaY} = req.body;
    console.log(req.body)
    Bicicleta.updateBici(new Bicicleta(id,color,modelo,[coordenadaX,coordenadaY]))
    res.redirect('/bicicleta')
    
}

exports.bicicleta_delete = (req,res)=>{
    Bicicleta.removeBici(req.body.id)
    res.redirect('/bicicleta')
}
