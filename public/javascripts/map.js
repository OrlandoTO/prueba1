var mymap = L.map('mapid').setView([36.634718, -81.790600], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { 
     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' 
    }).addTo(mymap); 

$.ajax({
     dataType:"json",
     url:"api/bicicleta",
     success: function(result){
          result.bicicletas.forEach(function(bicicleta){
               L.marker(bicicleta.ubicacion,{title:bicicleta.id}).addTo(mymap);
               
          })

     }
})